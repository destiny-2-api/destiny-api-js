module.exports = {
  entry: __dirname + '/index.js',
  output: {
      path: __dirname + '/dist',
      filename: 'destiny-api.js'
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /(node_modules|bower_components)/,
        use: {
          loader: 'babel-loader',
          options: {
            presets: ['env'],
            plugins: [require('babel-plugin-transform-object-rest-spread')]
          }
        }
      }
    ]
  }
};

