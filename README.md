# destiny-api-js

> A Javascript client for the Bungie.net Destiny 2 API usable on the server side or client side.

### Installation

``` bash
# using yarn
$ yarn add destiny-api-js

# using npm
$ npm install destiny-api-js --save
```

### Typical Usage

- First, create an application @ https://www.bungie.net/en/Application and save your API key
- Then create a new instance of the API client

```javascript
const { DestinyApiClient } = require('destiny-api-js')
const apiClient = new DestinyApiClient('your-api-key')
```

You can then make calls to endpoints that are documented here https://bungie-net.github.io/ like so:

```javascript
// Search for a member by name and platform
apiClient.Destiny2.SearchDestinyPlayer('Sabazou', -1)
```

Some things like BungieMembershipType or DestinyComponentType are annoying to remember and use so you can access them using the `schemas` object by requiring it like so:

```javascript

// Make the above call, but without a hardcoded -1
const { schemas } = require('destiny-api-js')

const membershipType = schemas.BungieMembershipType.All
apiClient.Destiny2.SearchDestinyPlayer('Sabazou', membershipType)
```

```javascript
// Get some data using async/await
const { schemas } = require('destiny-api-js')

const membershipType = schemas.BungieMembershipType.All
let { data } = await apiClient.Destiny2.SearchDestinyPlayer('Sabazou', membershipType)


// [{"iconPath":"/img/theme/destiny/icons/icon_xbl.png","membershipType":1,"membershipId":"4611686018448212231","displayName":"Sabazou"}]
console.log(data.Response)
```
### Usage on the browser side
You need to make sure you register the correct `Origin Header` in your app settings at https://www.bungie.net/en/Application otherwise you will get a CORS error when making requests.

### More information
For specifics relating the Bungie.net api consult the official documentation at https://bungie-net.github.io/

This library uses axios for XHR transports. Documentation for axios can be seen at https://github.com/axios/axios