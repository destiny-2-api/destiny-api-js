const DestinyApiClient = require('./src/DestinyApiClient')

const schemas = {
  DestinyComponentType: require('./src/schemas/DestinyComponentType'),
  BungieMembershipType: require('./src/schemas/BungieMembershipType')
}

module.exports = {
  DestinyApiClient,
  schemas
}