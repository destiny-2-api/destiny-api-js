module.exports = function (endpoints = [], httpClient) {
  let map = {}
  
  endpoints.forEach(endpoint => {
    map[endpoint.tag] = {}
    
    Object.keys(endpoint.methods).forEach(name => {
      map[endpoint.tag][name] = endpoint.methods[name](httpClient)
    })
  })
  
  return map
}