module.exports = function (httpClient) {
  return (q) => {
    return httpClient.get(`User/SearchUsers/`, {
      params: { q: encodeURIComponent(q) }
    })
  }
}
