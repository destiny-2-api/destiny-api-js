module.exports = function (httpClient) {
  return () => {
    return httpClient.get(`User/GetAvailableThemes/`)
  }
}
