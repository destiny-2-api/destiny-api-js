module.exports = function (httpClient) {
  return (userId) => {
    return httpClient.get(`User/GetUserAliases/${userId}/`)
  }
}
