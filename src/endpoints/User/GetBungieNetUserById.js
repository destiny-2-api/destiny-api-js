module.exports = function (httpClient) {
  return (userId) => {
    return httpClient.get(`User/GetBungieNetUserById/${userId}/`)
  }
}
