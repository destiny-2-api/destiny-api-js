module.exports = function (httpClient) {
  return ({ membershipId, membershipType }) => {
    return httpClient.get(`User/GetMembershipsById/${membershipId}/${membershipType}/`)
  }
}
