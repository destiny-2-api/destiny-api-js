const DestinyComponentType = require ('../../schemas/DestinyComponentType')

module.exports = function (httpClient) {
  return (membershipType, destinyMembershipId, components = [DestinyComponentType.Profiles]) => {
    return httpClient.get(`Destiny2/${membershipType}/Profile/${destinyMembershipId}/`, {
      params: {
        components: components.join(',')
      }
    })
  }
}