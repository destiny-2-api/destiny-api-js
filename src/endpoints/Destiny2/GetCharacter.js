module.exports = function (httpClient) {
  return ({ characterId, destinyMembershipId, membershipType, components = [] }) => {
    return httpClient.get(`Destiny2/${membershipType}/Profile/${destinyMembershipId}/Character/${characterId}/`, {
      params: {
        components: components.join(',')
      }
    })
  }
}