module.exports = function (httpClient) {
  return ({ entityType, hashIdentifier }) => {
    return httpClient.get(`Destiny2/Manifest/${entityType}/${hashIdentifier}/`)
  }
}