module.exports = function (httpClient) {
  return (membershipType, displayName) => {
    return httpClient.get(`Destiny2/SearchDestinyPlayer/${membershipType}/${displayName}/`)
  }
}
