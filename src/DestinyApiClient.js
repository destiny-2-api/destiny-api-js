const HttpClient = require('./HttpClient')
const mapEndpointMethods = require('./utils/mapEndpointMethods')
const endpoints = require('./endpoints')

const DestinyApiClient = function (apiKey, authRedirect) {
  if (!apiKey || typeof apiKey !== 'string') {
    throw new Error('DestinyApiClient constructor requires an apiKey string')
  }

  if (!authRedirect || typeof authRedirect !== 'string') {
    throw new Error('DestinyApiClient constructor requires an authRedirect string')
  }

  this.oAuthToken = null
  this.httpClient = new HttpClient({
    baseURL: 'https://www.bungie.net/Platform',
    apiKey
  })

  Object.assign(this, mapEndpointMethods(endpoints, this.httpClient))
}

DestinyApiClient.prototype = {
  setOAuthToken (token) {
    this.oAuthToken = token
    this.httpClient.defaults.headers.common['oauth2'] = token;
  }
}

module.exports = DestinyApiClient
