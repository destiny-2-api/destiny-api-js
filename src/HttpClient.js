const axios = require('axios')

module.exports = function ({ baseURL, apiKey }) {
  const instance = axios.create({
    baseURL
  })
  
  instance.defaults.headers.common['X-API-Key'] = apiKey;

  return instance
}